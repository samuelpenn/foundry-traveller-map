# Traveller Map Integration for FoundryVTT

A simple integration between Foundry and Traveller Map, allowing
world information to be displayed directly in Foundry.

You can install this in Foundry, either by copying in the zip file from
the release directory, and unzipping it in Data/modules, or by going to
'Install Module' in Foundry, and using the following Manifest URL:

```
https://gitlab.com/samuelpenn/foundry-traveller-map/-/raw/main/release/module.json
```

## Usage

### /uwp sector xxyy

You can specify a four character sector name, and four digit hex coordinate to
get details of an extact world:

![System List](docs/uwpjump.png)


### /uwp name

Alternatively, you can search for a named system. If more than one match is
found, then a list of systems will be shown. Clicking on any of the results
will bring up world details as before.

![System List](docs/list_nojump.png)

For examples:

```aidl
/uwp amondiage
/uwp colchis
/uwp new
```

### /route [\<jump>]

Display a route between two endpoints. Uses the default jump rating unless
a jump distance is specified.

You can define the destinations by clicking on the 'Add to route' link at
the bottom of each UWP result. The first click sets the start, the second
sets the destination.

Subsequent clicks will move the destination to the start, and set a new
destination.

`/route` will then calculate an efficient route between the two endpoints.
For short routes, an option will be provided to display it on the map.

![Route](docs/route.png)

## Configuration

#### Base Url

Defaults to the official TravellerMap site. Can be changed, just in case you
are running your own server.

#### Size of Inline Jump Map

If set to zero, an inline map is not displayed. Defaults to 2 parsecs.

#### Maximum number of search results

If multiple systems match the provided name, the maximum returned is defined
by this. Defaults to 10. Note that it's possible to specify single letter search
terms, in which case the number of results will be huge.

#### Whisper

By default, output is whispered to the current user. Alternatively, you can
have output go to everyone. Whispered output can also be made public on a
per-message basis, in the usual Foundry way.

#### Map style

Select the map style to use. This is one of the eight styles supported by
Traveller Map. This is set on the client, so different players can use
different styles. Popouts will inherit the style used in the chat message.

#### Show hex numbers

If selected, hex numbers will always be shown (depending on style) even in
empty hexes. This is off by default.

#### Map size

The size of the window to use when displaying a map in the inline browser.
Defaults to 'Medium', but can be set to 'Tiny', 'Small', 'Medium' or 'Large'.
The window can be resized after it has been opened, this is just the default
size.

#### Map Orientation

Default aspect ration for the map, either 'Landscape', 'Portrait' or 'Square'.

#### Default jump rating

Default jump distance to use when calculating routes. Defaults to 2, and can
be overridden when using the `/route` command.

#### Milieu

Define which date period to use for the map data. Defaults to 1105.
