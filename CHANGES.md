# Release Notes

## v1.1.0

* Added configurable to set the milieu for display and search.

## v1.0.0

* Incremented supported version to FVTT 12.

## v0.5.0

* Added /route command for displaying route on the map.

## v0.4.0

* Added inline web viewer to allow browsing of the entire map. Greatly inspired by https://gitlab.com/sparradee/traveller-map-macro

## v0.3.0

* First public release.
