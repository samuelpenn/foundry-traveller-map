import { UWP } from "./uwp.mjs";

export const JOURNAL = {};

JOURNAL.getFolder = async function(sectorName, create) {
    let folders = game.journal.directory.folders;
    let folder = null;
    let NAME = game.settings.get("traveller-map", "journalFolder");

    for (let i=0; i < folders.length; i++) {
        if (folders[i].name === NAME) {
            folder = folders[i];
            break;
        }
    }
    if (folder == null) {
        let folderData = {
            "name": NAME,
            "parent": folder,
            "type": "JournalEntry"
        }
        folder = await Folder.create(folderData);
    }

    let sectors = folder.getSubfolders();
    let sector = null;
    for (let s=0; s < sectors.length; s++) {
        if (sectors[s].name === sectorName) {
            sector = sectors[s];
            break;
        }
    }
    if (!sector) {
        let folderData = {
            "name": sectorName,
            "folder": folder,
            "type": "JournalEntry"
        }
        sector = await Folder.create(folderData);
    }

    return sector;
}

JOURNAL.createJournal = async function(j, html) {
    let sectorName = j.getAttribute("data-sector");
    let worldName = j.getAttribute("data-name");
    let worldXY = j.getAttribute("data-xy");

    UWP.displayHex(sectorName, worldXY, JOURNAL.createNewEntry);
};

JOURNAL.uwpToText = function(uwp) {
    let html = `<div class="uwp-big">`;

    for (let i of [ 0, 1, 2, 3, 4, 5, 6]) {
        html += `<p class="uwp-letter">${uwp.substring(i, i+1)}</p>`;
    }
    html += `<p class="uwp-dash"> - </p>`;
    html += `<p class="uwp-letter">${uwp.substring(8, 9)}</p>`;

    html += `</div>`;

    return html;
}

/**
 * When a journal page is edited, most HTML formatting is broken. Anything that
 * looks like a paragraph has a <p></p> tag wrapped around it. <span> tags are
 * removed or merged together. So the HTML we use to create the page has to be
 * safe from such breakages. Hence, using <p> tags instead of <li> etc.
 */
JOURNAL.createPageText = function(world) {
    let html = `<div class="uwp"><div class="uwp-page">`;

    console.log(world);

    if (world.Zone) {
        if (world.Zone === "A") {
            html += `<div class="zone amber-zone">CAUTION</div>`;
        } else if (world.Zone === "R") {
            html += `<div class="zone red-zone">RESTRICTED</div>`;
        }
    }

    let port = world.UWP[0];
    html += `<div class="starport">${port}</div>`;

    if (world.Bases) {
        for (let i=0; i < world.Bases.length; i++) {
            let base = world.Bases.substring(i, i+1);
            if (base === "N") {
                html += `<div class="base">Navy Base</div>`;
            } else if (base === "S") {
                html += `<div class="base">Scout Base</div>`;
            }
        }
    }

    html += `<span class="sector-name">${world.Sector} / ${world.SubsectorName} [${world.SS}] / ${world.Hex}</span><br/>`;
    html += `<span class="allegiance">${world.AllegianceName}</span><br/>`;

    // Star System Information
    html += `<h3>Star System</h3>`;

    html += `<p>${world.Stellar}</p>`;

    let belts = world.PBG.substring(1, 2);
    let gas = world.PBG.substring(2, 3);
    let other = world.Worlds;
    html += `<p>${gas} Gas Giants - ${belts} Planetoid Belts - ${other} Other Worlds`;

    html += `<p><b>Importance: </b>${UWP.getImportanceText(world.Ix)}</p>`;
    html += `<p class="nobility">${UWP.getNobilityText(world.Nobility)}</p>`;

    html += `<h3>World Data</h3>`;

    // Display local jump map inline.
    let radius = game.settings.get('traveller-map', 'miniRadius');
    let HOST = game.settings.get('traveller-map', 'mapUrl');
    let MILIEU = game.settings.get('traveller-map', 'milieu');
    let style = game.settings.get('traveller-map', 'style');
    let hex = game.settings.get('traveller-map', 'hexNumbers')?"&allhexes=1":"";
    let mapUrl = `${HOST}/api/jumpmap?sector=${world.Sector}&milieu=${MILIEU}&hex=${world.Hex}&style=${style}&options=49392${hex}`;

    html += `<img class="journal-jump-map mini-jump-map" src='${mapUrl}&jump=${radius}' title="${world.Name}"/>`;

    html += JOURNAL.uwpToText(world.UWP);
    html += UWP.uwpToText(world.UWP, world.PBG, true);
    html += `${world.Remarks}<br/>`;
    html += `</div>`;

    if (game.settings.get('traveller-map', 'useCulture')) {
        html += `<div class="extra-info">`;
        let economics = world.Ex;
        let culture = world.Cx;

        html += `<h4>Economics</h4>`;

        html += `<p><b>Resources: </b> ${UWP.getResourcesText(world.Ex)}</p>`;
        html += `<p><b>Labour: </b> ${UWP.getLabourText(world.Ex)}</p>`;
        html += `<p><b>Infrastructure: </b> ${UWP.getInfrastructureText(world.Ex)}</p>`;
        html += `<p><b>Efficiency: </b> ${UWP.getEfficiencyText(world.Ex)}</p>`;

        html += `<h4>Culture</h4>`;

        html += `<p><b>Heterogeneity </b>: ${UWP.getHeterogeneityText(world.Cx)}</p>`;
        html += `<p><b>Acceptance </b>: ${UWP.getAcceptanceText(world.Cx)}</p>`;
        html += `<p><b>Strangeness </b>: ${UWP.getStrangenessText(world.Cx)}</p>`;
        html += `<p><b>Symbols </b>: ${UWP.getSymbolsText(world.Cx)}</p>`;
        html += `<p><b>Symbols </b>: ${UWP.getSymbolsText(world.Cx)}</p>`;

        html += `</div>`;
    }

    html += `<div style="clear:both"></div>`;
    html += "<hr/>";



    html += `</div>`;
    return html;
}

JOURNAL.createNewEntry = async function(obj, sectorName, worldXY) {
    console.log("createNewEntry:");
    let folder = await JOURNAL.getFolder(sectorName, true);

    if (folder === null) {
        console.log("No such folder");
        return;
    }

    const world = obj.Worlds[0];
    let worldName = `${world.Hex} ${world.Name}`

    let format = game.settings.get("traveller-map", "worldNameFormat");
    if (format === "NNN") {
        worldName = `${world.Name}`;
    } else if (format === "NNNXY") {
        worldName = `${world.Name} ${world.Hex}`;
    } else if (format === "XYNNN") {
        worldName = `${world.Hex} ${world.Name}`;
    }

    let journalEntry = null;
    for (let f=0; f < folder.contents.length; f++) {
        let j = folder.contents[f];
        if (j.name === worldName) {
            journalEntry = j;
            break;
        }
    }

    if (journalEntry) {
        console.log(journalEntry);
        journalEntry.show();
        return;
    }

    if (!journalEntry) {
        let journalData = {
            "name": worldName,
            "folder": folder,
            "type": "JournalEntry",
            "ownership": {
                "default": 3
            }
        }
        journalEntry = await JournalEntry.create(journalData);
    }
    console.log(journalEntry);
    let pages = await journalEntry.pages;

    let pageData = [
        {
            name:  `${world.Name} / ${sectorName} ${world.Hex}`,
            text: {
                content: JOURNAL.createPageText(world)
            },
            sort: 10,
            ownership: {
                default: 2
            }
        }
    ];

    if (game.settings.get("traveller-map", "addPlayerPage")) {
        pageData.push(
            {
                name: 'Player Notes',
                text: {content: "Player notes go here"},
                sort: 20,
                ownership: {
                    default: 3
                }
            }
        );
    }
    if (game.settings.get("traveller-map", "addGMPage")) {
        pageData.push(
            {
                name: "GM Notes",
                    text: { content: "GM only notes" },
                sort: 30,
                    ownership: {
                        default: 0
                    }
            }
        );
    }

    const worldPage = await journalEntry.createEmbeddedDocuments("JournalEntryPage", pageData);
    journalEntry.show();
}