
// noinspection JSUnresolvedVariable

export const UWP = {};

UWP.routeStart = null;
UWP.routeEnd = null;

// World code tables taken from Traveller Map:
// https://github.com/inexorabletash/travellermap/blob/5071401455137dfb36596427a1378ee746a3dae6/world_util.js
UWP.ATMOSPHERE = {
    0: 'No atmosphere',
    1: 'Trace',
    2: 'Very thin; Tainted',
    3: 'Very thin',
    4: 'Thin; Tainted',
    5: 'Thin',
    6: 'Standard',
    7: 'Standard; Tainted',
    8: 'Dense',
    9: 'Dense; Tainted',
    A: 'Exotic',
    B: 'Corrosive',
    C: 'Insidious',
    D: 'Dense, high',
    E: 'Thin, low',
    F: 'Unusual',
    X: 'Unknown',
    '?': 'Unknown'
}

UWP.GOVERNMENT = {
    0: 'No Government Structure',
    1: 'Company/Corporation',
    2: 'Participating Democracy',
    3: 'Self-Perpetuating Oligarchy',
    4: 'Representative Democracy',
    5: 'Feudal Technocracy',
    6: 'Captive Government / Colony',
    7: 'Balkanization',
    8: 'Civil Service Bureaucracy',
    9: 'Impersonal Bureaucracy',
    A: 'Charismatic Dictator',
    B: 'Non-Charismatic Dictator',
    C: 'Charismatic Oligarchy',
    D: 'Religious Dictatorship',
    E: 'Religious Autocracy',
    F: 'Totalitarian Oligarchy',
    G: 'Small Station or Facility',
    H: 'Split Clan Control',
    J: 'Single On-world Clan Control',
    K: 'Single Multi-world Clan Control',
    L: 'Major Clan Control',
    M: 'Vassal Clan Control',
    N: 'Major Vassal Clan Control',
    P: 'Small Station or Facility',
    Q: 'Krurruna or Krumanak Rule for Off-world Steppelord',
    R: 'Steppelord On-world Rule',
    S: 'Sept',
    T: 'Unsupervised Anarchy',
    U: 'Supervised Anarchy',
    W: 'Committee',
    X: 'Unknown',
    '?': 'Unknown'
};

UWP.LAW = {
    0: 'No prohibitions',
    1: 'Body pistols, explosives, and poison gas prohibited',
    2: 'Portable energy weapons prohibited',
    3: 'Machine guns, automatic rifles prohibited',
    4: 'Light assault weapons prohibited',
    5: 'Personal concealable weapons prohibited',
    6: 'All firearms except shotguns prohibited',
    7: 'Shotguns prohibited',
    8: 'Long bladed weapons controlled; open possession prohibited',
    9: 'Possession of weapons outside the home prohibited',
    A: 'Weapon possession prohibited',
    B: 'Rigid control of civilian movement',
    C: 'Unrestricted invasion of privacy',
    D: 'Paramilitary law enforcement',
    E: 'Full-fledged police state',
    F: 'All facets of daily life regularly legislated and controlled',
    G: 'Severe punishment for petty infractions',
    H: 'Legalized oppressive practices',
    J: 'Routinely oppressive and restrictive',
    K: 'Excessively oppressive and restrictive',
    L: 'Totally oppressive and restrictive',
    S: 'Special/Variable situation',
    X: 'Unknown',
    '?': 'Unknown'
};

UWP.TECH = {
    0: 'Stone Age',
    1: 'Bronze, Iron',
    2: 'Printing Press',
    3: 'Basic Science',
    4: 'External Combustion',
    5: 'Mass Production',
    6: 'Nuclear Power',
    7: 'Miniaturized Electronics',
    8: 'Quality Computers',
    9: 'Anti-Gravity',
    A: 'Interstellar community',
    B: 'Lower Average Imperial',
    C: 'Average Imperial',
    D: 'Above Average Imperial',
    E: 'Above Average Imperial',
    F: 'Technical Imperial Maximum',
    G: 'Robots',
    H: 'Artificial Intelligence',
    J: 'Personal Disintegrators',
    K: 'Plastic Metals',
    L: 'Comprehensible only as technological magic',
    X: 'Unknown',
    '?': 'Unknown'
};

UWP.IMPORTANCE = {
    "-3": { "label": "Very unimportant", "weekly": 0, "daily": 0 },
    "-2": { "label": "Very unimportant", "weekly": 0, "daily": 0 },
    "-1": { "label": "Unimportant", "weekly": 1, "daily": 0 },
    "0": { "label": "Unimportant", "weekly": 2, "daily": "1" },
    "1": { "label": "Ordinary", "weekly": 10, "daily": "1-2" },
    "2": { "label": "Ordinary", "weekly": 20, "daily": "2-4" },
    "3": { "label": "Ordinary", "weekly": 30, "daily": "3-6" },
    "4": { "label": "Important", "weekly": 100, "daily": "15-20" },
    "5": { "label": "Very important", "weekly": 1000, "daily": "100" }
};

UWP.RESOURCES = {
    "2": "Very Scarce",
    "3": "Very Scarce",
    "4": "Scarce",
    "5": "Scarce",
    "6": "Few",
    "7": "Few",
    "8": "Moderate",
    "9": "Moderate",
    "A": "Abundant",
    "B": "Abundant",
    "C": "Very abundant",
    "D": "Very abundant",
    "E": "Extremely abundant",
    "F": "Extremely abundant",
    "G": "Extremely abundant",
    "H": "Extremely abundant",
    "J": "Extremely abundant"
};

UWP.LABOUR = {
    0: 'Unpopulated',
    1: 'Tens',
    2: 'Hundreds',
    3: 'Thousands',
    4: 'Tens of thousands',
    5: 'Hundreds of thousands',
    6: 'Millions',
    7: 'Tens of millions',
    8: 'Hundreds of millions',
    9: 'Billions',
    A: 'Tens of billions',
    B: 'Hundreds of billions',
    C: 'Trillions',
    D: 'Tens of trillions',
    E: 'Hundreds of tillions',
    F: 'Quadrillions',
    X: 'Unknown',
    '?': 'Unknown'
};

UWP.INFRASTRUCTURE = {
    0: 'Non-existent',
    1: 'Extremely limited',
    2: 'Extremely limited',
    3: 'Very limited',
    4: 'Very limited',
    5: 'Limited',
    6: 'Limited',
    7: 'Generally available',
    8: 'Generally available',
    9: 'Extensive',
    A: 'Extensive',
    B: 'Very extensive',
    C: 'Very extensive',
    D: 'Comprehensive',
    E: 'Comprehensive',
    F: 'Very comprehensive',
    G: 'Very comprehensive',
    H: 'Very comprehensive',
    '?': 'Unknown'
};

UWP.EFFICIENCY = {
    '-5': 'Extremely poor',
    '-4': 'Very poor',
    '-3': 'Poor',
    '-2': 'Fair',
    '-1': 'Average',
    '0': 'Average',
    '+1': 'Average',
    '+2': 'Good',
    '+3': 'Improved',
    '+4': 'Advanced',
    '+5': 'Very advanced',
    '?': 'Unknown'
};

UWP.HETEROGENEITY = {
    0: 'N/A',
    1: 'Monolithic',
    2: 'Monolithic',
    3: 'Monolithic',
    4: 'Harmonious',
    5: 'Harmonious',
    6: 'Harmonious',
    7: 'Discordant',
    8: 'Discordant',
    9: 'Discordant',
    A: 'Discordant',
    B: 'Discordant',
    C: 'Fragmented',
    D: 'Fragmented',
    E: 'Fragmented',
    F: 'Fragmented',
    G: 'Fragmented',
    '?': 'Unknown'
};

UWP.ACCEPTANCE = {
    0: 'N/A',
    1: 'Extremely xenophobic',
    2: 'Very xenophobic',
    3: 'Xenophobic',
    4: 'Extremely aloof',
    5: 'Very aloof',
    6: 'Aloof',
    7: 'Aloof',
    8: 'Friendly',
    9: 'Friendly',
    A: 'Very friendly',
    B: 'Extremely friendly',
    C: 'Xenophilic',
    D: 'Very Xenophilic',
    E: 'Extremely xenophilic',
    F: 'Extremely xenophilic',
    '?': 'Unknown'
};

UWP.STRANGENESS = {
    0: 'N/A',
    1: 'Very typical',
    2: 'Typical',
    3: 'Somewhat typical',
    4: 'Somewhat distinct',
    5: 'Distinct',
    6: 'Very distinct',
    7: 'Confusing',
    8: 'Very confusing',
    9: 'Extremely confusing',
    A: 'Incomprehensible',
    '?': 'Unknown'
};

UWP.STRANGENESS = {
    0: 'Extremely concrete',
    1: 'Extremely concrete',
    2: 'Very concrete',
    3: 'Very concrete',
    4: 'Concrete',
    5: 'Concrete',
    6: 'Somewhat concrete',
    7: 'Somewhat concrete',
    8: 'Somewhat abstract',
    9: 'Somewhat abstract',
    A: 'Abstract',
    B: 'Abstract',
    C: 'Very abstract',
    D: 'Very abstract',
    E: 'Extremely abstract',
    F: 'Extremely abstract',
    G: 'Extremely abstract',
    H: 'Incomprehensibly abstract',
    J: 'Incomprehensibly abstract',
    K: 'Incomprehensibly abstract',
    L: 'Incomprehensibly abstract',
    '?': 'Unknown'
};

UWP.SYMBOLS = {
    0: 'Extremely concrete',
    1: 'Extremely concrete',
    2: 'Very concrete',
    3: 'Very concrete',
    4: 'Concrete',
    5: 'Concrete',
    6: 'Somewhat concrete',
    7: 'Somewhat concrete',
    8: 'Somewhat abstract',
    9: 'Somewhat abstract',
    A: 'Abstract',
    B: 'Abstract',
    C: 'Very abstract',
    D: 'Very abstract',
    E: 'Extremely abstract',
    F: 'Extremely abstract',
    G: 'Extremely abstract',
    H: 'Incomprehensibly abstract',
    J: 'Incomprehensibly abstract',
    K: 'Incomprehensibly abstract',
    L: 'Incomprehensibly abstract',
    '?': 'Unknown'
};

UWP.NOBILITY = {
    B: 'Knight',
    c: 'Baronet',
    C: 'Baron',
    D: 'Marquis',
    e: 'Viscount',
    E: 'Count',
    f: 'Duke',
    F: 'Subsector Duke',
    G: 'Archduke',
    H: 'Emperor',
    '?': 'Unknown'
}


UWP.printNumber = function (number, precision) {
    if (number === 0) {
        return 0;
    }
    number = parseFloat(number);
    if (precision === null || precision === undefined || precision < 0) {
        precision = 2;
    }

    if (number > 1e12 || number < 1e-3) {
        return number.toExponential(precision);
    } else if (number > 99) {
        return Number(parseInt(number)).toLocaleString();
    } else {
        return number.toPrecision(precision);
  }
};


UWP.help = function(chatData) {
    let text = `<div class="uwp">`;
    text += `<h2>Traveller Map Tools</h2>`;

    text += `/uwp &lt;world name><br/>`;
    text += `/uwp &lt;sector> &lt;xxyy><br/>`;
    text += `/route [&lt;jump>]<br/><br/>`;

    text += `</div>`;

    chatData.content = text;
    ChatMessage.create(chatData);
};

UWP.getText = function(code, table) {
    if (code) {
        if (table[code]) {
            return `(${code}) ${table[code]}`;
        } else {
            return `(${code}) ?`;
        }
    } else {
        return "(?)";
    }
}

UWP.getAtmosphereText = function(code) {
    return UWP.getText(code, UWP.ATMOSPHERE);
};

UWP.getGovernmentText = function(code) {
    return UWP.getText(code, UWP.GOVERNMENT);
};

UWP.getLawText = function(code) {
    return UWP.getText(code, UWP.LAW);
}

UWP.getTechText = function(code) {
    return UWP.getText(code, UWP.TECH);
}

UWP.getImportanceText = function(code) {
    if (!code || code.split(" ").length < 1) {
        return "?";
    }
    code = code.split(" ")[1];
    let o = UWP.IMPORTANCE[code];
    if (!o) {
        console.log("No importance information for [" + code +"]");
        return "?";
    }
    let text = `${o.label}`;

    if (o.weekly !== 0) {
        text += ` - ${o.weekly} ships/week`;
    }
    if (o.daily !== 0) {
        text += ` - ${o.daily} ships/day`;
    }
    return text;
}

UWP.getEconomicsCode = function(code, idx) {
    if (!code || idx < 0 || idx > code.length) {
        return null;
    }
    return code.substring(idx, idx+1);
}

UWP.getResourcesText = function(code) {
    return UWP.getText(UWP.getEconomicsCode(code, 1), UWP.RESOURCES);
}
UWP.getLabourText = function(code) {
    return UWP.getText(UWP.getEconomicsCode(code, 2), UWP.LABOUR);
}
UWP.getInfrastructureText = function(code) {
    return UWP.getText(UWP.getEconomicsCode(code, 3), UWP.INFRASTRUCTURE);
}
UWP.getEfficiencyText = function(code) {
    if (code) {
        code = code.substring(4,6);
        return UWP.getText(code, UWP.EFFICIENCY);
    }
    return "?";
}

UWP.getCultureCode = function(code, idx) {
    if (!code || idx < 0 || idx > code.length) {
        return null;
    }
    return code.substring(idx, idx+1);
}

UWP.getHeterogeneityText = function(code) {
    return UWP.getText(UWP.getCultureCode(code, 1), UWP.HETEROGENEITY);
}
UWP.getAcceptanceText = function(code) {
    return UWP.getText(UWP.getCultureCode(code, 2), UWP.ACCEPTANCE);
}
UWP.getStrangenessText = function(code) {
    return UWP.getText(UWP.getCultureCode(code, 3), UWP.STRANGENESS);
}
UWP.getSymbolsText = function(code) {
    return UWP.getText(UWP.getCultureCode(code, 4), UWP.SYMBOLS);
}

UWP.getNobilityText = function(codes) {
    if (!codes) {
        return "";
    }
    let text = "";
    for (let i=0; i < codes.length; i++) {
        let code = codes.substring(i, i+1);
        let rank = UWP.getText(code, UWP.NOBILITY);
        if (text) {
            text += " &mdash; ";
        }
        text += rank;
    }
    return text;
}



UWP.uwpToText = function(uwp, pbg, journal) {
    let text = `<div class="uwp-block">`;

    let starport = uwp.substring(0, 1);
    let size = parseInt(uwp.substring(1, 2), 16) * 1600;
    let atmosphere = uwp.substring(2, 3);
    let hydrographics = parseInt(uwp.substring(3, 4), 16);
    let population = parseInt(uwp.substring(4, 5), 16);
    let government = uwp.substring(5, 6);
    let law = uwp.substring(6, 7);
    let tech = uwp.substring(8, 9);

    let popDigit = parseInt(pbg.substring(0, 1), 16);
    if (population > 0) {
        population = parseInt(popDigit * Math.pow(10, population));
    } else {
        population = 0;
    }

    if (journal) {
        text += `<p><b>Starport:</b> ${starport}</p>`;
        text += `<p><b>Size:</b> ${UWP.printNumber(size)}km</p>`;
        text += `<p><b>Atmosphere:</b> ${UWP.getAtmosphereText(atmosphere)}</p>`;
        text += `<p><b>Hydrographics:</b> ${hydrographics * 10}%</p>`;
        text += `<p><b>Population:</b> ${UWP.printNumber(population)}</p>`;
        text += `<p><b>Government:</b> ${UWP.getGovernmentText(government)}</p>`;
        text += `<p><b>Law Level:</b> ${UWP.getLawText(law)}</p>`;
        text += `<p><b>Tech Level:</b> ${UWP.getTechText(tech)}</p>`;
    } else {
        text += "<dl class='traveller-map-uwp'>";
        text += `<dt>Starport</dt><dd>${starport}</dd>`;
        text += `<dt>Size</dt><dd>${UWP.printNumber(size)}km</dd>`;
        text += `<dt>Atmosphere</dt><dd>${UWP.getAtmosphereText(atmosphere)}</dd>`;
        text += `<dt>Hydrographics</dt><dd>${hydrographics * 10}%</dd>`;
        text += `<dt>Population</dt><dd>${UWP.printNumber(population)}</dd>`;
        text += `<dt>Government</dt><dd>${UWP.getGovernmentText(government)}</dd>`;
        text += `<dt>Law Level</dt><dd>${UWP.getLawText(law)}</dd>`;
        text += `<dt>Tech Level</dt><dd>${UWP.getTechText(tech)}</dd>`;
        text += "</dl>";
    }
    text += "</div>";
    return text;
}

UWP.uwpCommand = function(chatData, args) {
    if (args < 1) {
        return;
    }

    if (args.length === 2 && args[0].length === 4 && args[1].length === 4) {
        // Sector and sector coord
        let sector = args.shift();
        let xy = args.shift();

        UWP.displayHex(sector, xy);
    } else {
        let query = ""
        while (args.length > 0) {
            if (query.length > 0) {
                query += " ";
            }
            query += args.shift();
        }
        UWP.displaySearch(query);
    }
}

UWP.displaySearch = function(query) {
    let HOST = game.settings.get('traveller-map', 'mapUrl');
    let MILIEU = game.settings.get('traveller-map', 'milieu');
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", `${HOST}/api/search?milieu=${MILIEU}&q=${escape(query)}`);

    xmlHttp.onload = function () {
        if (xmlHttp.status === 200) {
            let obj = JSON.parse(xmlHttp.responseText);
            UWP.processSearchResults(obj);
        } else {
            ui.notifications.error("Unable to connect to TravellerMap");
        }
    }

    xmlHttp.onerror = function () {
        ui.notifications.error("Error connecting to TravellerMap");
    }
    xmlHttp.send();
}

UWP.chatMessage = function(text) {
    let whisper = game.settings.get('traveller-map', 'whisper')?[ game.userId ]: [ ];
    let chatData = {
        user: game.userId,
        speaker: ChatMessage.getSpeaker(),
        content: text,
        whisper: whisper
    }
    ChatMessage.create(chatData, {});
}

UWP.processSearchResults = function(obj) {
    if (obj.Results && obj.Results.Count > 0) {
        // If we have exactly one world, display that, otherwise display a list.
        // Some of the 'hits' can be sectors, so we might get > 1 result, but only
        // one is a world. It's probably always going to be the first entry, but
        // we allow for it not to be.
        let world = -1;
        for (let w=0; w < obj.Results.Items.length; w++) {
            if (obj.Results.Items[w].World) {
                if (world != -1) {
                    world = -1;
                    break;
                } else {
                    world = w;
                }
            }
        }
        if (world > -1) {
            let item = obj.Results.Items[world];
            let sector = escape(item.World.Sector);
            let x = (""+item.World.HexX).padStart(2, "0");
            let y = (""+item.World.HexY).padStart(2, "0");
            UWP.displayHex(sector, `${x}${y}`);
        } else if (obj.Results.Items) {
            let text = "<div class='uwp'><ul>";
            let max = game.settings.get('traveller-map', 'maxResults');
            for (let item of obj.Results.Items) {
                if (item.World) {
                    let sector = escape(item.World.Sector);
                    let x = (""+item.World.HexX).padStart(2, "0");
                    let y = (""+item.World.HexY).padStart(2, "0");

                    text += `<li class="world-item" data-sector="${sector}" data-hex="${x}${y}"><a>${item.World.Sector} / ${item.World.Name} (${x}${y})</a></li>`;
                    if (--max < 1) {
                        break;
                    }
                }
            }
            text += "</ul></div>";

            UWP.chatMessage(text);
        }
    } else {
        ui.notifications.warn("Unable to find any worlds matching that search");
        return;
    }
}

UWP.displayHex = function(sector, xy, func) {
    let HOST = game.settings.get('traveller-map', 'mapUrl');
    let MILIEU = game.settings.get('traveller-map', 'milieu');
    let xmlHttp = new XMLHttpRequest();

    xmlHttp.open("GET", `${HOST}/data/${sector}/${xy}?milieu=${MILIEU}`, true);
    xmlHttp.onload = function () {
        if (xmlHttp.status === 200) {
            let obj = JSON.parse(xmlHttp.responseText);
            if (func) {
                func(obj, sector, xy);
            } else {
                UWP.processHexResults(obj, sector, xy);
            }
        } else {
            ui.notifications.error("Unable to connect to TravellerMap");
        }
    }

    xmlHttp.onerror = function () {
        ui.notifications.error("Error connecting to TravellerMap");
    }
    xmlHttp.send();

}

UWP.processHexResults = function(obj, sector, xy) {
    if (obj.Worlds.length == 0) {
        ui.notifications.warn("No world found at location ${xy}");
        return;
    }

    let data = obj.Worlds[0];
    let radius = game.settings.get('traveller-map', 'miniRadius');
    let HOST = game.settings.get('traveller-map', 'mapUrl');
    let MILIEU = game.settings.get('traveller-map', 'milieu');
    let style = game.settings.get('traveller-map', 'style');
    let hex = game.settings.get('traveller-map', 'hexNumbers')?"&allhexes=1":"";

    let mapUrl = `${HOST}/api/jumpmap?sector=${data.Sector}&milieu=${MILIEU}&hex=${data.Hex}&style=${style}&options=49392${hex}`;
    let fullUrl = `${HOST}/?sector=${data.Sector}&milieu=${MILIEU}&hex=${data.Hex}&style=${style}&options=58367${hex}`;

    let text = `<div class="uwp"><div class="uwp-text">`;
    text += `<h2>${data.Name}</h2>`;
    text += `<span class="subsector">${data.SubsectorName} Subsector</span><br/>`;
    text += `<span class="sector-name">${data.Sector} ${data.Hex}</span><br/>`;
    text += `<span class="allegiance">${data.AllegianceName}</span><br/><br/>`;
    text += `${data.UWP}<br/>`;
    text += `${data.Remarks}<br/>`;
    let uwp = data.UWP;
    text += UWP.uwpToText(uwp, data.PBG);
    text += "</div>";
    if (radius > 0) {
        text += `<img class="mini-jump-map" src='${mapUrl}&jump=${radius}' title="${data.Name}" data-url="${fullUrl}"/>`;
    } else {
        text += `<a class="mini-jump-map" title="${data.Name}" data-url="${fullUrl}">View on map</a>`;
    }
    text += "<ul class='jump-links'>";
    for (let j=1; j <= 6; j++) {
        text += `<li><a class="mini-jump-map" title="${data.Name}" data-jump="${j}" data-url="${mapUrl}">J-${j}</a></li>`;
    }
    text += "</ul>";

    text += `<div class="centre-link">`;
    text += `<a class="uwp-route-add" data-sector="${data.SectorAbbreviation}" \ 
            data-name="${data.Name}" data-xy="${data.Hex}" \
            title="Use /route to calculate a route">Add to route</a>`;

    if (game.settings.get("traveller-map", "enableJournal")) {
        text += ` | <a class='uwp-journal' data-sector="${data.Sector}" data-name="${data.Name}" data-xy="${xy}">Create Journal</a>`;
    }
    text += "</div></div>";

    UWP.chatMessage(text);
}

UWP.routeCommand = function(chatData, args) {
    let jump = parseInt(game.settings.get('traveller-map', 'defaultJump'));
    let MILIEU = game.settings.get('traveller-map', 'milieu');
    if (args.length > 0) {
        jump = parseInt(args.shift())
    }

    if (!UWP.routeStart || !UWP.routeEnd) {
        ui.notifications.error("You need to first specify a start and destination for your route");
        return;
    }
    let startSec = UWP.routeStart.replaceAll(/ .*/g, "");
    let startHex = UWP.routeStart.replaceAll(/.* /g, "");
    let endSec = UWP.routeEnd.replaceAll(/ .*/g, "");
    let endHex = UWP.routeEnd.replaceAll(/.* /g, "");

    const HOST = game.settings.get('traveller-map', 'mapUrl');
    let xmlHttp = new XMLHttpRequest();
    const url = `${HOST}/api/route?milieu=${MILIEU}&start=${startSec}+${startHex}&end=${endSec}+${endHex}&jump=${jump}`;

    xmlHttp.open("GET", url, true);
    xmlHttp.onload = function () {
        if (xmlHttp.status === 200) {
            let obj = JSON.parse(xmlHttp.responseText);
            UWP.processRouteResults(obj, jump);
        } else {
            ui.notifications.error("Unable to find route");
        }
    }

    xmlHttp.onerror = function () {
        ui.notifications.error("Error connecting to TravellerMap");
    }
    xmlHttp.send();
}

UWP.processRouteResults = function(obj, jump) {
    let text = "<div class='uwp'>";

    let start = obj[0];
    let end = obj[obj.length -1];

    text += `<h2>J-${jump} Route</h2>`;
    text += `<p>From ${start.Name} ${start.Hex}/${start.Subsector}/${start.Sector} `;
    text += `to ${end.Name} ${end.Hex}/${end.Subsector}/${end.Sector}</p>`;

    text += "<ul>";
    //    /route troj 2616 troj 3215 2
    let route = { "Tour": true, "Route": true, "Results": { Items: [] }};
    for (let i=0; i < obj.length; i++) {
        let world = obj[i];
        text += `<li>${world.Name} ${world.Hex}</li>`;

        let o = { "Name": obj[i].Name, "SectorX": obj[i].SectorX, "SectorY": obj[i].SectorY, "HexX": obj[i].HexX, "HexY": obj[i].HexY };
        let w = { World: o };
        route.Results.Items.push(w);
    }
    text += "</ul>";
    const HOST = game.settings.get('traveller-map', 'mapUrl');
    let MILIEU = game.settings.get('traveller-map', 'milieu');
    let url = `${HOST}/?milieu=${MILIEU}&qr=${encodeURIComponent(JSON.stringify(route))}`;
    if (url.length > 2048) {
        text += `<p><i>Route too long for map</i></p>`;
    } else {
        text += `<a class="centre-link mini-jump-map" title="Route" data-url="${url}">View on map</a>`;
    }
    text += "</div>";
    UWP.chatMessage(text);
}